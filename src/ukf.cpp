/*
* Copyright (c) 2016 Carnegie Mellon University, Author <achamber>
*
* For License information please see the LICENSE file in the root directory.
*
*/

/*
 * ukf.cpp
 *
 *  Created on: Jan 9, 2012
 *      Author: achamber
 */

#include "ukf/ukf.h"
#include <ukf/chiSqTable.h>
#include <iostream>
#include <iomanip>
ca_ukf::ca_ukf(int xSize, int P_Size, int n_Size, int Q_Size)
{
  x = Eigen::VectorXd::Zero(xSize,1);
  P = Eigen::MatrixXd::Zero(P_Size, P_Size);
  n = Eigen::VectorXd::Zero(n_Size,1);
  Q = Eigen::MatrixXd::Zero(Q_Size, Q_Size);

  srand((unsigned)time(0));

#ifdef EIGEN_VECTORIZE
  //std::cerr << "ukf: EIGEN_VECTORIZE is properly defined" << std::endl;
#else
  std::cerr << "ukf: EIGEN_VECTORIZE is NOT defined. You should enable sse2 or greater to improve performance..." << std::endl;
#endif

//  pathLogFile = "/home/achamber/ukfLog.txt";
//  if (!pathLogFile.empty())
}

ca_ukf::~ca_ukf()
{
//  if (!logFile.is_open())
//    logFile.close();
}

Eigen::MatrixXd ca_ukf::calculateSigmaPoints(const Eigen::VectorXd &x, const Eigen::MatrixXd &P, const double lambda)
{
  // N: Number of elements in the state vector
  int N = x.rows();

  // Perform a robust Cholesky decomposition on the covariance matrix
  Eigen::LDLT<Eigen::MatrixXd> ldlt((lambda+N)*P);

  // If any diagonal terms are negative, set them to zero
  Eigen::VectorXd  v= ldlt.vectorD();
  for (int i = 0; i < N; i++)
  {
    if (v(i) < 0)
    {
      v(i) = 0;
    }
  }

  // Take the square root of the diagonal
  Eigen::MatrixXd Dsq = Eigen::MatrixXd::Zero(N,N);
  Dsq.diagonal() = v.array().sqrt();

  // Combine all the pieces of the decomposition to get the lower triangular matrix
  Eigen::MatrixXd S = ldlt.transpositionsP().transpose()*Eigen::MatrixXd(ldlt.matrixL())*Dsq;

  // Initialize the matrix of sigma points
  Eigen::MatrixXd xSigma(N, 2*N+1);

  // Create each sigma point
  xSigma.col(0) = x;
  for (int i = 1; i <= N; i++)
  {
    xSigma.col(i) = x + S.col(i-1);
    xSigma.col(N+i) = x - S.col(i-1);
  }

  return xSigma;
}

void ca_ukf::predictUKF(const double alpha, const double beta)
{

  // Create Augmented state vector
  Eigen::VectorXd xAug(x.rows()+n.rows());
  // std::cout<<"state rows "<<x.rows()<<" noise rows "<<n.rows()<<std::endl;
  xAug << x, n;

  // N: Number of elements in the state vector
  int N = xAug.rows();

  // Create the Augmented covariance matrix
  Eigen::MatrixXd PAug = Eigen::MatrixXd::Zero(N, N);
  PAug.block(0,0,P.rows(),P.cols()) = P;
  PAug.block(P.rows(), P.cols(), Q.rows(), Q.cols()) = Q;


  double lambda = alpha*alpha*((double)N ) - (double)N;

  // Calculate sigma points
  Eigen::MatrixXd xSigma = calculateSigmaPoints(xAug, PAug, lambda);
  // Calculate weights
  double W0m = lambda / (lambda+N);
  double W0c = W0m + (1 - alpha*alpha + beta);
  double Wjm = 1 / (2*(lambda+N));
  double Wjc = Wjm;
  double W_temp=N/(lambda+N); 
 
  // std::cout<<"inside ukf --"<< xSigma.cols()<<std::endl;
  // Pass sigma points through process function f
  Eigen::MatrixXd xSigmaK1(x.rows(), xSigma.cols());
  for (int i = 0; i < xSigma.cols(); i++)
  {
    xSigmaK1.col(i) = predictionModel(xSigma.col(i));
  }
  // Find the weighted sum of the sigma points
  Eigen::VectorXd xK1 = W0m*xSigmaK1.col(0);
  xK1.noalias() += (Wjm * xSigmaK1.block(0,1, xSigmaK1.rows(), xSigmaK1.cols()-1).rowwise().sum());

  // Find the updated covariance matrix
  Eigen::MatrixXd PK1 = W0c*(xSigmaK1.col(0) - xK1)*(xSigmaK1.col(0) - xK1).transpose();
  for (int i = 1; i < xSigmaK1.cols(); i++)
  {
    PK1.noalias() += Wjc*((xSigmaK1.col(i) - xK1)*(xSigmaK1.col(i) - xK1).transpose());
  }
  // Update the state vector and covariance matrix to the new values
  x = xK1;
  P = PK1;
}

bool ca_ukf::correctUKF(const Eigen::VectorXd &z, const Eigen::MatrixXd &R, const double alpha, const double beta, const double kappa, const double errorThreshold)
{

  constructMeasurement(z, alpha, beta, kappa);

  // Calculate Kalman gain
  Eigen::MatrixXd S_inv = (Pzz+R).inverse();
  Eigen::MatrixXd K = Pxz*S_inv;

  // Measurement residual
  Eigen::VectorXd v = z - zPred;

  // Chi square innovation test
  if (errorThreshold > 0)
  //	if (0)
  {
    static int chiSqProb_col = (int)((errorThreshold - CHI_SQ_TABLE_PROB_MIN)/CHI_SQ_TABLE_INCREMENT) + 1;
    double error = v.transpose()*S_inv*v;

    double maxErrorAllowed = chiSqTable[z.size()][chiSqProb_col];

  //		if (!logFile.is_open())
  //				logFile << maxErrorAllowed << " " << error << std::endl;

    if (error > maxErrorAllowed)
    {
      std::cerr << "Measurement: " << z.transpose() << std::endl;
      std::cerr << "Allowed error: " << maxErrorAllowed << std::endl;
      std::cerr << "Innovation error: " << error << std::endl;

      return false;
    }
  }

  // Correction update equations
  Eigen::VectorXd x_plus = x + K*v;
  Eigen::MatrixXd P_plus = P - K*(Pzz+R)*(K.transpose());

  // Update the state vector and covariance matrix to the new values
  x = x_plus;
  P = P_plus;

  return true;
}


void ca_ukf::constructMeasurement(const Eigen::VectorXd &z, const double alpha, const double beta, const double kappa)
{
  // N: Number of elements in the state vector
  int N = x.rows();

  double lambda = alpha*alpha*(N + kappa) - N;

  // Calculate sigma points
  Eigen::MatrixXd xSigma = calculateSigmaPoints(x, P, lambda);

  // Calculate weights
  double W0m = lambda / (lambda+N);
  double Wjm = 1 / (2*(lambda+N));

  double W0c = W0m + (1 - alpha*alpha + beta);
  double Wjc = Wjm;


  // Pass sigma points through measurement function measureFunc
  Eigen::MatrixXd gamma(z.rows(), xSigma.cols());
  for (int i = 0; i < xSigma.cols(); i++)
  {
    gamma.col(i) = measurementModel(xSigma.col(i));
  }

  // Find the weighted sum of the sigma points
  zPred = W0m*gamma.col(0);
  zPred.noalias() += (Wjm * gamma.block(0, 1, gamma.rows(), gamma.cols()-1).rowwise().sum());

  Pxz = W0c*(xSigma.col(0) - x)*(gamma.col(0) - zPred).transpose();
  Pzz = W0c*(gamma.col(0) - zPred)*(gamma.col(0) - zPred).transpose();

  for (int i = 1; i < gamma.cols(); i++)
  {
    Pxz.noalias() += Wjc*(xSigma.col(i) - x)*(gamma.col(i) - zPred).transpose();
    Pzz.noalias() += Wjc*(gamma.col(i) - zPred)*(gamma.col(i) - zPred).transpose();
  }
}


double ca_ukf::rand_normal(void)
{
  float r1 = (float)rand()/(float)RAND_MAX;
  float r2 = (float)rand()/(float)RAND_MAX;
  float r_normal = sqrt(-2*log(r1))*cos(2*M_PI*r2);
  return r_normal;
}
