/*
 * simple.cpp
 *
 *  Created on: Jan 9, 2012
 *      Author: achamber
 */

// Simple example of the UKF

// 1D movement
// We tell the process model the current acceleration
// Measurement is the position


#include <iostream>
#include <ukf/ukf.h>
using namespace std;

class Simple_ukf : public ca_ukf
{
public:
	Simple_ukf(int xSize, int P_Size, int n_Size, int Q_Size): ca_ukf(xSize, P_Size, n_Size, Q_Size) { };
	virtual Eigen::VectorXd predictionModel(Eigen::VectorXd x);
	virtual Eigen::VectorXd measurementModel(Eigen::VectorXd x);

	double u;
	double dt;
};

Eigen::VectorXd Simple_ukf::predictionModel(Eigen::VectorXd x)
{

	Eigen::VectorXd xk1(2);

	double measuredAccel = u;
	double trueAccel = measuredAccel - x(2);

	xk1(0) = x(0) + x(1)*dt + 0.5*trueAccel*dt*dt;
	xk1(1) = x(1) + trueAccel*dt;

	return xk1;
}

Eigen::VectorXd Simple_ukf::measurementModel(Eigen::VectorXd x)
{
	Eigen::VectorXd z_measured(1);
	z_measured(0) = x(0);

	return z_measured;
}


int main() {

	Simple_ukf filt(2,2,1,1);
	filt.P << 0.01, 0,
			  0, 0.01;

	cout << "x:\n" << filt.x << endl;
	cout << "P:\n" << filt.P << endl;

	// Prediction cycle

	filt.dt = 0.1; // Change in time
	// IMU mechanization
	filt.u = 0.2;  // Measured acceleration
	filt.n(0) = 0; // Mean noise in acceleration measurement
	filt.Q(0,0) = 0.05; // Covariance in acceleration measurement

	double alpha = 0.01;
	double beta = 3;

	int numSteps = 10;
	double const_u = 0.2;

	Eigen::VectorXd true_x = filt.x;

	for (int i = 0; i < numSteps; i++)
	{


		filt.u = const_u + filt.rand_normal()*filt.Q(0,0);

		filt.predictUKF(alpha, beta);

		true_x(0) = true_x(0) + true_x(1)*filt.dt + 0.5*const_u*filt.dt*filt.dt;
		true_x(1) = true_x(1) + const_u*filt.dt;

		Eigen::VectorXd measured_z(1);
		Eigen::MatrixXd R(1,1);
		R(0,0) = 0.01;

		measured_z(0) = true_x(0) + filt.rand_normal()*R(0,0);
		filt.correctUKF(measured_z, R, alpha, beta);


	}

	cout << "true x:\n" << true_x << endl;
	cout << "filtered x:\n" << filt.x << endl;


	return 0;
}
