/*********************************************************************
 *
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2011, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Sebastian Klose
 *********************************************************************/

#ifndef IMU_FILTER_MESSAGE_HELPER_H
#define IMU_FILTER_MESSAGE_HELPER_H

#include <Eigen/Eigen>
#include <Eigen/Geometry>

#include <boost/array.hpp>

#include <sensor_msgs/Imu.h>
#include <geometry_msgs/PoseStamped.h>

namespace imu_filter 
{
	//* General helper functions for message conversions and other conversion
	namespace message_helper
	{
		/*!
		 *	\brief	convert from boost array to eigen matrix
		 */ 
	/*
		template<typename T, size_t r, size_t c>
		static inline void matrixPtrToEigen( const boost::array<T, r*c> & matPtr, Eigen::Matrix<T, r, c> & mat )
		{
			for( size_t row = 0; row < r; row++ )
				for( size_t col = 0; col < c; col++ )
					mat( row, col ) = matPtr[ row * c + col ];
		}
		*/
		template<typename T, int r, int c, size_t a>
		static inline void matrixPtrToEigen(const boost::array<T, a> & matPtr, Eigen::Matrix<T, r, c> & mat )
		{
			for( int row = 0; row < r; row++ )
				for( int col = 0; col < c; col++ )
					mat( row, col ) = matPtr[ row * c + col ];
		}

		template<typename Derived, typename T, size_t a>
		static inline Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> arrayToEigenMat(const boost::array<T, a> & bArray, int r, int c)
		{

			assert(a == r*c);

			Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> eigMat(r, c);

			for( int row = 0; row < r; row++ )
			{
				for( int col = 0; col < c; col++ )
				{
					eigMat(row, col) = bArray[ row * c + col ];
				}
			}
		}


		template<typename Derived, typename T, size_t a>
		static inline void eigenToArray(const Eigen::MatrixBase<Derived> & mat, boost::array<T, a> & matPtr)
		{

			int r = mat.rows();
			int c = mat.cols();

			assert(a == r*c);

			for( int row = 0; row < r; row++ )
			{
				for( int col = 0; col < c; col++ )
				{
					matPtr[ row * c + col ] = (T)mat(row, col);
				}
			}
		}

		/*
		template<typename T, size_t a, typename Derived>
		static inline void matrixPtrToEigen(const boost::array<T, a> & matPtr, Eigen::DenseBase<Derived>& mat )
		{
			size_t r = mat.rows();
			size_t c = mat.cols();
			for( size_t row = 0; row < r; row++ )
				for( size_t col = 0; col < c; col++ )
					mat( row, col ) = matPtr[ row * c + col ];
		}
				*/
		/*!
		 *	\brief	retrieve the quaternion from an IMU message	
		 */ 
		static inline void quaternionFromImuMsg( const sensor_msgs::Imu::ConstPtr & msg, Eigen::Quaterniond & q )
		{
			q.x() = msg->orientation.x;
			q.y() = msg->orientation.y;
			q.z() = msg->orientation.z;
			q.w() = msg->orientation.w;
		}	

		/*!
		 *	\brief	retrieve the angular rate from an IMU message	
		 */ 
		static inline void angularRateFromImuMsg( const sensor_msgs::Imu::ConstPtr & msg, Eigen::Vector3d & v )
		{
			v.x() = msg->angular_velocity.x;
			v.y() = msg->angular_velocity.y;
			v.z() = msg->angular_velocity.z;
		}

		/*!
		 *	\brief	retrieve the linear acceleration from an IMU message	
		 */ 
		static inline void linearAccelerationFromImuMsg( const sensor_msgs::Imu::ConstPtr & msg, Eigen::Vector3d & v )
		{
			v.x() = msg->linear_acceleration.x;
			v.y() = msg->linear_acceleration.y;
			v.z() = msg->linear_acceleration.z;
		}

		/*!
		 *	\brief	convert Eigen Quaternion to geometry_msgs::Quaternion
		 */ 
		static inline void quaternionToMsg( const Eigen::Quaterniond & q, geometry_msgs::Quaternion & msg )
		{
			msg.x = q.x();
			msg.y = q.y();
			msg.z = q.z();
			msg.w = q.w();
		}

		/*!
		 *	\brief	Eigen::Vector3d to geometry_msgs::Vector3	
		 */ 
		static inline void vec3ToMsg( const Eigen::Vector3d & v, geometry_msgs::Vector3 & msg )
		{
			msg.x = v.x();
			msg.y = v.y();
			msg.z = v.z();
		}

		/*!
		 *	\brief	Eigen::Affine3d to geometry_msgs::PoseStamped	
		 */ 
		static inline void eigenPose2Msg( const Eigen::Affine3d & pose, geometry_msgs::PoseStamped & msg  )
		{
			Eigen::Quaterniond q;
			q = pose.rotation();
			msg.pose.orientation.x = q.x();
			msg.pose.orientation.y = q.y();
			msg.pose.orientation.z = q.z();
			msg.pose.orientation.w = q.w();
			msg.pose.position.x = pose.translation().x();
			msg.pose.position.y = pose.translation().y();
			msg.pose.position.z = pose.translation().z();		
		}
	}
}

#endif
