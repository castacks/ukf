/*
 * ukf.h
 *
 *  Created on: Jan 9, 2012
 *      Author: achamber
 */

#ifndef CA_UKF_H_
#define CA_UKF_H_

//#include <fstream>
//#include <iostream>
#include <Eigen/Eigen>

class ca_ukf
{
public:
	ca_ukf(int xSize, int P_Size, int n_Size, int Q_Size);
	virtual ~ca_ukf();
	virtual Eigen::VectorXd predictionModel(const Eigen::VectorXd &x) = 0;
	virtual Eigen::VectorXd measurementModel(const Eigen::VectorXd &x) = 0;

	static Eigen::MatrixXd calculateSigmaPoints(const Eigen::VectorXd &x, const Eigen::MatrixXd &P, const double lambda);
	void predictUKF(const double alpha, const double beta);
	void constructMeasurement(const Eigen::VectorXd &z, const double alpha, const double beta, const double kappa = 0.00);
	bool correctUKF(const Eigen::VectorXd &z, const Eigen::MatrixXd &R, const double alpha, const double beta, const double kappa = 0.0, const double errorThreshold = 0.85);
	double rand_normal(void);

public:
	Eigen::VectorXd x;
	Eigen::MatrixXd P;
	Eigen::VectorXd n;
	Eigen::MatrixXd Q;

	Eigen::MatrixXd Pxz;
	Eigen::MatrixXd Pzz;
	Eigen::VectorXd zPred;

	//	std::ofstream logFile;
	//	std::string pathLogFile;

};


#endif /* CA_UKF_H_ */
